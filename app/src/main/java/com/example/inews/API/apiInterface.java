package com.example.inews.API;

import com.example.inews.models.NewsAPI;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface apiInterface {

    @GET("top-headlines")
    Call<NewsAPI> getTopHeadlines(
        @Query("country") String country,
        @Query("apiKey") String apiKey
    );


}
