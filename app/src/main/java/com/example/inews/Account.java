package com.example.inews;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Calendar;
import java.util.Date;

public class Account extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


        private FirebaseAuth mAuth;
        RelativeLayout layoutt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        View contextView = findViewById(R.id.content);

        String name = FirebaseAuth.getInstance().getCurrentUser().getDisplayName();
        TextView hello = (TextView)findViewById(R.id.username);
        hello.setText(name);

        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        TextView helloo = (TextView)findViewById(R.id.email);
        helloo.setText(email);


        layoutt = findViewById(R.id.layout);




    }


    public void logOut(View view){
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(this,FBLoginGoogleLogin.class));

    }


    protected void onStop(){
        super.onStop();
        SharedPreferences loginData = getSharedPreferences("StoredInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = loginData.edit();
        Date currentTime = Calendar.getInstance().getTime();
        editor.putString("info", "Welcome");
        editor.putString("data&time", String.valueOf(currentTime));

        editor.apply();

    }

    protected void onPause(){
        super.onPause();
        SharedPreferences loginData = getSharedPreferences("StoredInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = loginData.edit();
        Date currentTime = Calendar.getInstance().getTime();
        editor.putString("info", "Welcome");
        editor.putString("data&time", String.valueOf(currentTime));
        editor.apply();
    }

    protected void onStart(){
        super.onStart();
        SharedPreferences loginData = getSharedPreferences("StoredInfo", Context.MODE_PRIVATE);
        String name = loginData.getString("info", "");
        String date = loginData.getString("data&time", "");

        Snackbar.make(layoutt, name + " " + date, Snackbar.LENGTH_INDEFINITE).show();
    }

    protected void onResume(){
        super.onResume();
        SharedPreferences loginData = getSharedPreferences("StoredInfo", Context.MODE_PRIVATE);
        String name = loginData.getString("info", "");
        String date = loginData.getString("data&time", "");

        Snackbar.make(layoutt, name + " " + date, Snackbar.LENGTH_INDEFINITE).show();
    }



    public void onDestroy(){
        super.onDestroy();

    }





    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.nav_home) {

            startActivity(new Intent(this,MainNews.class));

        } else if (id == R.id.nav_slideshow) {

            startActivity(new Intent(this,Account.class));

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
