package com.example.inews;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import java.util.Calendar;
import java.util.Date;

import com.goodiebag.pinview.Pinview;

public class MainActivity extends AppCompatActivity {


    public void hello(){
        Pinview pinview = (Pinview)findViewById(R.id.pinview);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pinview, boolean fromUser) {

                if(pinview.getValue().equals("1234")){
                    Toast.makeText(MainActivity.this, "Redirecting...", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(MainActivity.this, FBLoginGoogleLogin.class);
                    startActivity(intent);
                }else{
                    Toast.makeText(MainActivity.this, "Invalid Pin", Toast.LENGTH_SHORT).show();

                }
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        hello();

    }








}
