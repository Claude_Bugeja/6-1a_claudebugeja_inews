package com.example.inews;

import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.support.annotation.NonNull;
import android.view.MenuItem;
import android.widget.TextView;

import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.example.inews.models.Article;
import com.example.inews.API.apiInterface;
import com.example.inews.API.NewsAPIClient;
import com.example.inews.models.NewsAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainNews extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String API_KEY = "ddb328de04874a97a2292d7bc878b601";
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private List<Article> articles = new ArrayList<>();
    private Adapter adapter;
    private String TAG = MainNews.class.getSimpleName();
    private TextView Website1;
    private TextView Website2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_news);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);


        recyclerView = findViewById(R.id.recylerView);
        layoutManager = new LinearLayoutManager(MainNews.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setNestedScrollingEnabled(false);

        Website1 = (TextView)findViewById(R.id.link1);
        Website2 = (TextView)findViewById(R.id.link2);

        asyncTask task = new asyncTask(MainNews.this);
        task.execute();

        LoadJson();
    }


    public void openLink(View view){

        String url1 = Website1.getText().toString();


        Uri webpage1 = Uri.parse(url1);


        Intent intent = new Intent(Intent.ACTION_VIEW, webpage1);


        startActivity(intent);


    }


    public void openLink2(View view){


        String url2 = Website2.getText().toString();


        Uri webpage2 = Uri.parse(url2);


        Intent intent2 = new Intent(Intent.ACTION_VIEW, webpage2);


        startActivity(intent2);

    }

    public void LoadJson(){

        apiInterface ApiInterface = NewsAPIClient.getApiClient().create(apiInterface.class);






        Call<NewsAPI> call;
        call = ApiInterface.getTopHeadlines("us", API_KEY);

        call.enqueue(new Callback<NewsAPI>() {
            @Override
            public void onResponse(Call<NewsAPI> call, Response<NewsAPI> response) {
                if(response.isSuccessful() && response.body().getArticle() != null){

                    if(!articles.isEmpty()){
                        articles.clear();
                    }

                    articles = response.body().getArticle();
                    adapter = new Adapter(articles, MainNews.this);
                    recyclerView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                }
                else{
                    Toast.makeText(MainNews.this, "No Result", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<NewsAPI> call, Throwable t) {
                Toast.makeText(MainNews.this, "No Result", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_news, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {

            startActivity(new Intent(this,MainNews.class));

        } else if (id == R.id.nav_slideshow) {

            startActivity(new Intent(this,Account.class));

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private static class asyncTask extends AsyncTask<String, String, String>{

        private WeakReference<MainNews> activityWeakReference;

        asyncTask(MainNews activity) {
            activityWeakReference = new WeakReference<MainNews>(activity);
        }


        @Override
        protected String doInBackground(String... strings) {
            return "Async Task Toast";
        }

        @Override
        protected void onPostExecute(String hello) {
            super.onPostExecute(hello);


            MainNews activity = activityWeakReference.get();

            Toast.makeText(activity, hello, Toast.LENGTH_LONG).show();

        }
    }

}
